"""
This script is the main script of the GREAT pipeline.

GREAT (Grapevine Expression Atlas) is a tool which recover all Grapevine RNA-seq public data and perform some analyses :

1. quality data validation (fastQC)
2. RNA-seq data alignment (STAR) on CRIBI annotation
3. gene quantification (featureCounts)
4. some statistics on the analyses

GREAT allows to have an overview of all the Grapevine transcriptomes available in the scientific commsampley and to have a look at a large choice of tissues/samples.

Example of command to launch a test run with GREAT : Go to the Snakefile directory, fill GREAT_config.json file and launch command : snakemake -p --cores 10
"""

###################################################################################################################
# SCRIPT'S HEADER
#------------------------------------------------------------------------------------------------------------------
__author__ = "Amandine Velt"
__license__ = "INRA"
__version__ = "1.0.0"
__maintainer__ = "Amandine Velt"
__email__ = "amandine.velt@inrae.fr"
__status__ = "Production"
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# TO PRINT IF RUN WORKED (onsuccess) OR NOT (onerror)
#------------------------------------------------------------------------------------------------------------------
onsuccess:
    print("GREAT workflow successfully finished !")

onerror:
    print("An error occurred during the GREAT workflow !")
#------------------------------------------------------------------------------------------------------------------

configfile: "/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_config.json"

###################################################################################################################
# DEFINE THE TARGET RULE (which allows to run all the rules of the script for each sample)
#------------------------------------------------------------------------------------------------------------------
rule all:
    input:
        "REPORT/report.html",
        expand("FASTQC/{sample}", sample=config["samples"]),
        expand("SALMON/{sample}/", sample=config["samples"]),
        expand("STATS/stats_{sample}.txt", sample=config["samples"])
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# CONVERT SRA TO FASTQ
#------------------------------------------------------------------------------------------------------------------
# If length of input_file == 2, we consider that it's paired-end data and we pass the SRA to FASTQ conversion step.
# If length of input_file == 1, we check if there the format is ".sra". If not, we pass the SRA to FASTQ conversion step.
# If there is .sra extension, we convert SRA to FASTQ.
# If length of input_file is different of 1 or 2, I kill the job, .json file is false.

rule sra_to_fastq:
    input:
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"]
    output:
        protected("CONVERSION/conversion_{sample}.txt")
    params:
        PESE=lambda wildcards: config["samples"][wildcards.sample]["PE_SE"]
    message: "Conversion of SRA in FASTQ on the following file : {input.input_file} in {params.PESE} mode."
    run:
      import subprocess
      import os, fnmatch
      import sys
      if len(input.input_file) == 2:
        shell("echo 'Files are already paired-end fastq files' > {output}")
      elif len(input.input_file) == 1:
        ext = os.path.splitext(str(list({input.input_file})[0]))[1]
        PESEok=str(params.PESE)
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        if (ext == ".sra" and not list_files):
          if (PESEok == "['SE']"):
            shell("echo 'File are in single-end format for sratoolkit conversion'")
            dump_command="fastq-dump  --outdir " + dir + " --gzip " + str(list({input.input_file})[0])
            subprocess.call(dump_command, shell=True)
          elif (PESEok == "['PE']"):
            shell("echo 'File are in paired-end format for sratoolkit conversion'")
            dump_command="fastq-dump --split-e --outdir " + dir + " --gzip " + str(list({input.input_file})[0])
            subprocess.call(dump_command, shell=True)
            remove_command="ls " + str(list({input.input_file})[0]) + " | sed 's/.sra/.fastq.gz/'"
            print(remove_command)
            remove=subprocess.getoutput(remove_command)
            #shell("rm -f {remove} || true")
          else:
            shell("echo 'PROBLEM : SRA toolkit don't know type of file : PE or SE !!!")
          shell("echo 'SRA file converted in fastq file(s)' > {output}")
        else:
          shell("echo 'File format is already fastq' > {output}")
      else:
        sys.exit("Input file length different of 1 or 2, EXIT!")

###################################################################################################################
# FASTQC ANALYSIS
#------------------------------------------------------------------------------------------------------------------
rule fastqc:
    input:
        "CONVERSION/conversion_{sample}.txt",
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"]
    output:
        directory("FASTQC/{sample}")
    threads: 2
    params:
        sample=lambda wildcards: config["samples"]
    message: "Executing fastQC analysis with {threads} threads on the following file : {input.input_file}."
    log:
        "LOGS/{sample}.FASTQC.log"
    run:
      import subprocess
      import os, fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
      else:
        list_files=input.input_file
      if len(list_files) == 2:
        fastq_command="foo=" + str(list_files[0]) + "; echo \"${foo%_*}\""
        fastq_name=subprocess.getoutput(fastq_command)
        #fastqR1=list_files[0]
        #fastqR2=list_files[1]
        shell("mkdir -p {output}/R1; fastqc -t {threads} -q -o {output}/R1 {fastq_name}_1.fastq.gz 2> {log}")
        shell("mkdir -p {output}/R2; fastqc -t {threads} -q -o {output}/R2 {fastq_name}_2.fastq.gz 2> {log}")
      else:
        shell("touch {log}; mkdir {output}; fastqc -t {threads} -q -o {output} {list_files} 2> {log}")
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# STAR MAPPING
#------------------------------------------------------------------------------------------------------------------
# 1. STAR uses annotations to extract known splice junctions and then builds "spliced" sequences by deleting intron
# sequences, i.e., joining the sequences of the exons. This is highly recommended, since it  allows  for  a  more
# accurate  mapping  of  the  spliced  reads,  especially those with very short junction overhangs (<10 nt).
# --outFilterMismatchNmax 999 -> because we want to consider mismatch in function of the reads length, not as a fixed number
# so I wrote 999 to not filter the alignments on this option (by default, at 10 mismatchs the alignment is removed)
# genes quantifications with STAR : https://www.biostars.org/p/218995/

rule star_map:
    input:
        "CONVERSION/conversion_{sample}.txt",
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"],
        genome_dir=config["references"]["genome"]
    output:
        "STAR/{sample}/{sample}"
    threads: 4
    message: "Executing STAR mapping with {threads} threads on the {input.input_file} file with {input.genome_dir} genome path."
    log:
        "LOGS/{sample}.STAR.log"
    run:
      import subprocess
      import os, fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
      else:
        list_files=input.input_file
      if len(list_files) >= 2:
        fastq_command="foo=" + str(list_files[0]) + "; echo \"${foo%_*}\""
        fastq_name=subprocess.getoutput(fastq_command)
        shell("touch {log}; STAR --outMultimapperOrder Random --outSAMtype BAM Unsorted --outSAMunmapped Within --readFilesCommand zcat --runThreadN {threads} --outFilterMismatchNmax 999 --outSAMprimaryFlag OneBestScore --outSAMattributes All --genomeDir {input.genome_dir} \
         --readFilesIn {fastq_name}_1.fastq.gz {fastq_name}_2.fastq.gz --outFileNamePrefix {output}_ 2> {log}; touch {output}")
      else:
        shell("touch {log}; STAR --outMultimapperOrder Random --outSAMtype BAM Unsorted --outSAMunmapped Within --readFilesCommand zcat --runThreadN {threads} --outFilterMismatchNmax 999 --outSAMprimaryFlag OneBestScore --outSAMattributes All --genomeDir {input.genome_dir} \
         --readFilesIn {list_files} --outFileNamePrefix {output}_ 2> {log}; touch {output}")
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# SAM PARSING
#------------------------------------------------------------------------------------------------------------------
# for each read, we keep only the best alignment
# and we keep only alignment with edit distance > edit_distance parameter given in the .json file

rule sam_parsing:
    input:
        sam="STAR/{sample}/{sample}"
    params:
        edit_distance=lambda wildcards: config["samples"][wildcards.sample]["edit_distance"]
    log:
        "LOGS/{sample}.STAR_PARSED.log"
    output:
        protected("STAR_PARSED/{sample}/{sample}.sorted.bam")
    message: "Executing STAR parsing on the {input.sam}_Aligned.out.sam file with edit_distance={params.edit_distance}."
    benchmark:
        "BENCHMARKS/{sample}.star_parsing.benchmark.txt"
    shell:
      # samtools view -F 0x100 -> allows to keep the best alignment of each read only
      # bamtools filter -tag \"NM:<={params.edit_distance}\" -> allows to remove alignment with large edit distance
      "samtools view -f 4 {input.sam}_Aligned.out.bam > {input.sam}_Aligned.unmapped.sam; "
      "samtools view -F 0x100 -b {input.sam}_Aligned.out.bam | samtools sort -o {output}.tmp -T $( pwd ) -; bamtools filter -tag \"NM:<={params.edit_distance}\" -in {output}.tmp -out {output}; rm {output}.tmp"
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# featureCounts ANALYSIS WITH CRIBI ANNOTATIONS
#------------------------------------------------------------------------------------------------------------------
rule featureCounts_CRIBI_annotations:
    input:
        "CONVERSION/conversion_{sample}.txt",
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"],
        bam="STAR_PARSED/{sample}/{sample}.sorted.bam",
        gff=config["references"]["annotations_CRIBI"]
    log:
        "LOGS/{sample}.FEATURE_COUNTS_CRIBI.log"
    benchmark:
        "BENCHMARKS/{sample}.featureCounts_CRIBI_annotations.benchmark.txt"
    params:
        attribute="GeneID",
        stranded=lambda wildcards: config["samples"][wildcards.sample]["protocol_library_featureCounts"]
    threads: 4
    output:
        complete_featureCounts_output=protected("FEATURE_COUNTS/CRIBI/FEATURE_COUNTS_{sample}.txt"),
        featureCounts_output_for_R=protected("FEATURE_COUNTS/CRIBI/FOR_R/FEATURE_COUNTS_{sample}.txt")
    message: "Executing featureCounts analysis on the {input.bam} file with {input.gff} annotations and with attribute={params.attribute}, stranded={params.stranded}."
    run:
      import subprocess
      import os, fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
      else:
        list_files=input.input_file
      if len(list_files) == 2:
        shell("featureCounts -p --primary -T {threads} -a {input.gff} -o {output.complete_featureCounts_output} -g {params.attribute} -s {params.stranded} {input.bam} 2> {log}; cut -f1,7 {output.complete_featureCounts_output} | grep -v '^#' | grep -v '^Geneid' > {output.featureCounts_output_for_R}; if [ ! -f FEATURE_COUNTS/CRIBI/FOR_R/genes_length.txt ]; then cut -f1,6 {output.complete_featureCounts_output} | grep -v '^#' > FEATURE_COUNTS/CRIBI/FOR_R/genes_length.txt; fi")
      else:
        shell("featureCounts --primary -T {threads} -a {input.gff} -o {output.complete_featureCounts_output} -g {params.attribute} -s {params.stranded} {input.bam} 2> {log}; cut -f1,7 {output.complete_featureCounts_output} | grep -v '^#' | grep -v '^Geneid' > {output.featureCounts_output_for_R}; if [ ! -f FEATURE_COUNTS/CRIBI/FOR_R/genes_length.txt ]; then cut -f1,6 {output.complete_featureCounts_output} | grep -v '^#' > FEATURE_COUNTS/CRIBI/FOR_R/genes_length.txt; fi")
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# GENERATION OF A TABLE WITH ALIGNMENT STATS ON SAMPLE
#------------------------------------------------------------------------------------------------------------------
rule stats_alignment_quantification_table_from_STAR:
    input:
        sam="STAR/{sample}/{sample}",
        featureCounts_CRIBI="FEATURE_COUNTS/CRIBI/FEATURE_COUNTS_{sample}.txt",
        bam="STAR_PARSED/{sample}/{sample}.sorted.bam",
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"]
    log:
        "LOGS/{sample}.STATS.log"
    benchmark:
        "BENCHMARKS/{sample}.stats_alignment_from_STAR.benchmark.txt"
    output:
        protected("STATS/stats_{sample}.txt")
    message:
      "Executing stats alignment table generation on the {input.sam}_Log.final.out file."
    run:
      import subprocess
      import os, fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
      else:
        list_files=input.input_file
      if len(list_files) == 2:
        shell("nb_pairs=$( grep 'Number of input reads' {input.sam}_Log.final.out | cut -d '|' -f2 | sed 's/\t//g' ); nb_reads=$(($nb_pairs*2)); \
        bamtools stats -in {input.bam} > stats_STAR_after_parsing_tmp_{wildcards.sample}.txt; \
        average_reads_length=$( grep 'Average input read length' {input.sam}_Log.final.out | cut -d '|' -f2 | sed 's/\t//g' ); \
        nb_reads_mapped_after_parsing=$( grep 'Mapped reads' stats_STAR_after_parsing_tmp_{wildcards.sample}.txt | cut -f1 | sed 's/.* //' ); \
        percent_reads_mapped_after_parsing=$( awk -v nbreads=\"$nb_reads_mapped_after_parsing\" -v total=\"$nb_reads\" 'BEGIN {{ pc=100*nbreads/total; i=int(pc); print (pc-i<0.5)?i:i+1 }}' ); \
        nb_reads_assigned_to_a_CRIBI_gene=$(grep \"Successfully assigned\" LOGS/{wildcards.sample}.FEATURE_COUNTS_CRIBI.log | cut -f2 -d\":\" | cut -f1 -d\"(\" | sed 's/ //g' ); \
        percent_reads_assigned_to_a_CRIBI_gene=$(grep \"Successfully assigned\" LOGS/{wildcards.sample}.FEATURE_COUNTS_CRIBI.log | cut -f2 -d\":\" | cut -f2 -d\"(\" | sed 's/ //g' | sed 's/%).*//' ); \
        rm stats_STAR_after_parsing_tmp_{wildcards.sample}.txt; \
        echo \"{wildcards.sample}\t$nb_reads\t$average_reads_length\t$nb_reads_mapped_after_parsing\t$percent_reads_mapped_after_parsing\t$nb_reads_assigned_to_a_CRIBI_gene\t$percent_reads_assigned_to_a_CRIBI_gene\" >> {output}")
      else:
        shell("nb_reads=$( grep 'Number of input reads' {input.sam}_Log.final.out | cut -d '|' -f2 | sed 's/\t//g' ); \
        bamtools stats -in {input.bam} > stats_STAR_after_parsing_tmp_{wildcards.sample}.txt; \
        average_reads_length=$( grep 'Average input read length' {input.sam}_Log.final.out | cut -d '|' -f2 | sed 's/\t//g' ); \
        nb_reads_mapped_after_parsing=$( grep 'Mapped reads' stats_STAR_after_parsing_tmp_{wildcards.sample}.txt | cut -f1 | sed 's/.* //' ); \
        percent_reads_mapped_after_parsing=$( awk -v nbreads=\"$nb_reads_mapped_after_parsing\" -v total=\"$nb_reads\" 'BEGIN {{ pc=100*nbreads/total; i=int(pc); print (pc-i<0.5)?i:i+1 }}' ); \
        nb_reads_assigned_to_a_CRIBI_gene=$(grep \"Successfully assigned\" LOGS/{wildcards.sample}.FEATURE_COUNTS_CRIBI.log | cut -f2 -d\":\" | cut -f1 -d\"(\" | sed 's/ //g' ); \
        percent_reads_assigned_to_a_CRIBI_gene=$(grep \"Successfully assigned\" LOGS/{wildcards.sample}.FEATURE_COUNTS_CRIBI.log | cut -f2 -d\":\" | cut -f2 -d\"(\" | sed 's/ //g' | sed 's/%).*//' ); \
        rm stats_STAR_after_parsing_tmp_{wildcards.sample}.txt; \
        echo \"{wildcards.sample}\t$nb_reads\t$average_reads_length\t$nb_reads_mapped_after_parsing\t$percent_reads_mapped_after_parsing\t$nb_reads_assigned_to_a_CRIBI_gene\t$percent_reads_assigned_to_a_CRIBI_gene\" >> {output}")
      rm_bam = config["rm_bam"]
      if rm_bam == "yes":
        shell("rm -f {input.sam}_Aligned.out.bam {input.bam}")
      import subprocess
      import fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        #shell("rm {input.input_file}")
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
       # shell("rm {list_files}")
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# GREAT REPORT GENERATION
#------------------------------------------------------------------------------------------------------------------
rule GREAT_report:
    input:
        featureCounts_cribi=expand("FEATURE_COUNTS/CRIBI/FOR_R/FEATURE_COUNTS_{sample}.txt", sample=config["samples"]),
        gff_cribi=config["references"]["annotations_CRIBI"],
        fastqc_results=expand("FASTQC/{sample}", sample=config["samples"]),
        stats_results=expand("STATS/stats_{sample}.txt", sample=config["samples"])
    message: "Executing GREAT report."
    output:
        "REPORT/report.html"
    run:

      import os, os.path
      import zipfile

      scriptsdir_path = config["scriptsdir"]

      count_files_dir="FEATURE_COUNTS/CRIBI/FOR_R/"
      count_files=len([name for name in os.listdir(count_files_dir)])
      if count_files > 1:
        shell("Rscript {scriptsdir_path}/RNA-seq_normalization_annotation.R -f FEATURE_COUNTS/CRIBI/FOR_R/ \
        -a {input.gff_cribi} -o FEATURE_COUNTS/CRIBI/ -g 'NULL' -l FEATURE_COUNTS/CRIBI/FOR_R/genes_length.txt")
      else:
        print("No normalization because one sample !")

      def zipdir(path, ziph):
      # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
          for file in files:
            ziph.write(os.path.join(root, file))

      zipf = zipfile.ZipFile('REPORT/FEATURE_COUNTS.zip', 'w', zipfile.ZIP_DEFLATED)
      zipdir('FEATURE_COUNTS/', zipf)
      zipf.close()
      FEATURE_COUNTS_zip="REPORT/FEATURE_COUNTS.zip"

      zipf = zipfile.ZipFile('REPORT/FASTQC.zip', 'w', zipfile.ZIP_DEFLATED)
      zipdir('FASTQC/', zipf)
      zipf.close()
      FASTQC_zip="REPORT/FASTQC.zip"

      shell("echo -ne \"Samplename\tNb reads\tAverage reads length\tNumber of reads mapped after parsing\t% of reads mapped after parsing\tNumber of reads/fragments assigned to CRIBI annotation genes\t% reads/fragments assigned to CRIBI annotation genes\n\" > STATS/all_samples_statistics.txt")
      shell("cat STATS/stats* >> STATS/all_samples_statistics.txt")

      from snakemake.utils import report

      report("""
      GREAT analysis report
      ===================================

      Reads were mapped with STAR to the PN40024 reference genome and genes quantification was done with featureCounts.
      You can access to the quality analysis with fastQC of all the data in zip file T1_.
      You can access to all the featureCounts files + normalization data table in zip file T2_.
      """, output[0], T1=FASTQC_zip, T2=FEATURE_COUNTS_zip)
#------------------------------------------------------------------------------------------------------------------


###################################################################################################################
#------------------------------------------------------------------------------------------------------------------
# New functionnality : Salmon run and report
#------------------------------------------------------------------------------------------------------------------

###################################################################################################################
# Salmon run
#------------------------------------------------------------------------------------------------------------------
# Salmon is a wicked-fast program to produce a highly-accurate, transcript-level quantification estimates from
# RNA-seq data. Salmon achieves is accuracy and speed via a number of different innovations, including the use of
# quasi-mapping (an accurate but fast-to-compute proxy for traditional read alignments) a two-phase inference procedure
# that makes use of massively-parallel stochastic collapsed variational inference, and extensive bias modeling to
# account for many of the manifold technical biases that can arise in RNA-seq samples (e.g., sequence-specific,
# position-specific, and fragment-GC content biases).

rule salmon_run:
    input:
        "CONVERSION/conversion_{sample}.txt",
        input_file=lambda wildcards: config["samples"][wildcards.sample]["input_file"],
        transcriptome_file=config["references"]["transcriptome_salmon"]
    output:
        directory("SALMON/{sample}")
    threads: 4
    message: "Executing SALMON with {threads} threads on the {input.input_file} file with {input.transcriptome_file} transcriptome path."
    log:
        "LOGS/{sample}.SALMON.log"
    run:
      import subprocess
      import os, fnmatch
      ext = os.path.splitext(str(list({input.input_file})[0]))[1]
      if ext == ".sra":
        dir_command="dirname " + str(list({input.input_file})[0])
        dir=subprocess.getoutput(dir_command)
        list_files1=fnmatch.filter(os.listdir(dir), '*.fastq.gz')
        list_files = [ dir + "/" + x for x in list_files1]
      else:
        list_files=input.input_file
      if len(list_files) >= 2:
        fastq_command="foo=" + str(list_files[0]) + "; echo \"${foo%_*}\""
        fastq_name=subprocess.getoutput(fastq_command)
        shell("touch {log}; salmon quant -i {input.transcriptome_file} -l A -p {threads} \
        -1 {fastq_name}_1.fastq.gz -2 {fastq_name}_2.fastq.gz -o {output} 2> {log};")
      else:
        shell("touch {log}; salmon quant -i {input.transcriptome_file} -l A -p {threads} \
        -r {list_files} -o {output} 2> {log};")
#------------------------------------------------------------------------------------------------------------------

# to aggregate transcript to gene with salmon : https://hbctraining.github.io/DGE_workshop_salmon/lessons/01_DGE_setup_and_overview.html
