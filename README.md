# GREAT RNAseq workflow installation (Linux)

```
cd /path/to/tools_folder
git clone https://gitlab.com/avelt/great_rnaseq_workflow.git
```

# First step : install miniconda3

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod 755 Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```

Responses to give during the Miniconda3 installation :
```
enter
yes
/path/to/tools_folder/miniconda3
no
```

# Second step : create the GREAT environment with all tools

```
export PATH=/path/to/tools_folder/miniconda3/bin:$PATH
conda update --all --yes
conda env create -f GREAT_environment.yaml
conda activate great
```

# Third step : install R packages

You can launch R inside the great environment and install the libraries manually OR these libraries will be installed during the first GREAT run.

```
R
install.packages("optparse")
install.packages("tools")
install.packages("locfit")
install.packages("BiocManager")
BiocManager::install("GenomicRanges")
BiocManager::install("shinyWidgets")
BiocManager::install("DESeq2")
BiocManager::install("GenomicFeatures")
BiocManager::install("genefilter")
BiocManager::install("geneplotter")
q()
```

# Fourth step : create genome indexes for STAR
```
mkdir /path/to/tools_folder/great_rnaseq_workflow/test_data/references_files/index
conda activate great

STAR --runThreadN 5 --runMode genomeGenerate --genomeDir test_data/references_files/index --genomeFastaFiles test_data/references_files/PN12Xv2.fa --sjdbGTFfile test_data/references_files/VCost.v3_20.formatted.gff3 --sjdbGTFtagExonParentTranscript Parent --sjdbGTFtagExonParentGene GeneID --genomeSAindexNbases 13
```

# Fifth step : fill GREAT/configuration files


## GREAT_config.json

Change following path with your path to the scriptsdir directory :

```"scriptsdir": "/path_to/great_rnaseq_workflow/scripts",```

Example : "scriptsdir": "/path/to/tools_folder/great_rnaseq_workflow/scripts",

Change following path with your path to the STAR indexes :

```"genome": "/path_to/great_rnaseq_workflow/test_data/references_files/index/",```

Example : "genome": "/path/to/tools_folder/great_rnaseq_workflow/test_data/references_files/index/",

Change following path with your path to the genome annotations (gff3) :

```"annotations_CRIBI": "/path_to/great_rnaseq_workflow/test_data/references_files/VCost.v3_20.formatted.gff3",```

Example : "annotations_CRIBI": "/path/to/tools_folder/great_rnaseq_workflow/test_data/references_files/VCost.v3_20.formatted.gff3",

Change following path with your path to the test samples :

```"test_sample_paired": {"input_file" : ["/path_to/great_rnaseq_workflow/test_data/samples/test_sample_paired.R1.fastq.gz","/path_to/great_rnaseq_workflow/test_data/samples/test_sample_paired.R2.fastq.gz"],"bioproject" : ["PRJNA1"],"first_author" : ["Velt"],"paper" : ["https://www.ncbi.nlm.nih.gov/pubmed/xxxxxxx"],"private_public" : ["Public"],"sequencing_platform" : ["Illumina"],"PE_SE" : ["PE"],"read_length" : ["151"],"protocol_library" : ["PolyA"],"stranded" : ["no"],"protocol_library_featureCounts" : ["0"],"species" : ["Vitis vinifera"],"variety" : ["Riesling"],"organ" : ["Leaves"],"tissue" : ["-"],"stage" : ["Young "],"comments" : ["accession 'Ventosa' - growth chamber, cuttings exposed to chilling and to freezing  temperatures"],"added_date" : ["fvr-18"],"treatment" : ["Yes"],"treatment_type" : ["Abiotic"],"treatment_nature" : ["Cold + frost"],"treatment_detail" : [""],"GREAT" : ["Yes"],"edit_distance" : ["7"]},```

Example : "test_sample_paired": {"input_file" : ["/path/to/tools_folder/great_rnaseq_workflow/test_data/samples/test_sample_paired.R1.fastq.gz","/path/to/tools_folder/great_rnaseq_workflow/test_data/samples/test_sample_paired.R2.fastq.gz"],"bioproject" : ["PRJNA1"],"first_author" : ["Velt"],"paper" : ["https://www.ncbi.nlm.nih.gov/pubmed/xxxxxxx"],"private_public" : ["Public"],"sequencing_platform" : ["Illumina"],"PE_SE" : ["PE"],"read_length" : ["151"],"protocol_library" : ["PolyA"],"stranded" : ["no"],"protocol_library_featureCounts" : ["0"],"species" : ["Vitis vinifera"],"variety" : ["Riesling"],"organ" : ["Leaves"],"tissue" : ["-"],"stage" : ["Young "],"comments" : ["accession 'Ventosa' - growth chamber, cuttings exposed to chilling and to freezing  temperatures"],"added_date" : ["fvr-18"],"treatment" : ["Yes"],"treatment_type" : ["Abiotic"],"treatment_nature" : ["Cold + frost"],"treatment_detail" : [""],"GREAT" : ["Yes"],"edit_distance" : ["7"]},

Do that for test_sample_single-end and SRR029281, too.

## GREAT_run.sh

`chmod 755 GREAT_run.sh`

Replace :

`conda activate /path_to/miniconda3/GREAT_environment`

By :

```
export PATH=/path/to/tools_folder/miniconda3/bin:$PATH
conda activate great
```

Replace : 

`cd /path_to/great_rnaseq_workflow/`

By :

`cd /path/to/tools_folder/great_rnaseq_workflow/`

Replace :

`snakemake --rerun-incomplete -s Snakefile --cores 4 &>> /path_to/great_rnaseq_workflow/GREAT.log`

By :

`snakemake --rerun-incomplete -s Snakefile --cores 4 &>> /path/to/tools_folder/great_rnaseq_workflow/GREAT.log`


# Final step : Test the installation with test data

`./GREAT_run.sh`
