#!/bin/bash

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate great

export PERL5LIB=/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/envs/great/lib/5.26.2/


cd /home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/test_data
snakemake --rerun-incomplete -s /home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/Snakefile --cores 4
