#!/bin/bash

# example : ./download_SRA_files.sh /data2/GREAT/great_rnaseq_workflow/test_data/liste_SRR.txt

liste="$1"
# module load sratoolkit/2.10.8

cd /data2/GREAT/GREAT_rawdata/
DATE=$(date +"%d_%b_%G")

if [ -z "$liste" ]
then
echo 'The first argument is empty. Command example : ./download_SRA_files.sh ../test_data/liste_SRR.txt' ; exit 1;
fi

FIELDS="fastq_md5,fastq_ftp"

for SRR in `cat $liste`
do
ACCESSION=$SRR
echo "$ACCESSION $FIELDS"
curl -s "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=$FIELDS&accession=$ACCESSION" | sed 1d >> $DATE.ena.metadata.tsv
done

for SRR in `cat $liste`
do
if [ -f "$DATE.ena.metadata.tsv" ]
then
  test=$( grep $SRR $DATE.ena.metadata.tsv | cut -f3 )
  if echo $test | grep -q ";"
  then
    fastqR1=$( grep $SRR $DATE.ena.metadata.tsv | cut -f3 | cut -f1 -d";" )
    fastqR2=$( grep $SRR $DATE.ena.metadata.tsv | cut -f3 | cut -f2 -d";" )
    if [ -z "${fastqR1}" ]
    then
      echo "fastqR1 doesn't exist"
    else
      echo "wget $fastqR1" >> fastq_a_telecharger.sh
    fi
    if [ -z "${fastqR2}" ]
    then
    echo "fastqR2 doesn't exist"
    else
    echo "wget $fastqR2" >> fastq_a_telecharger.sh
    fi
  else
    fastqR1=$( grep $SRR $DATE.ena.metadata.tsv | cut -f3 | cut -f1 -d";" )
    if [ -z "${fastqR1}" ]
    then
      echo "fastqR1 doesn't exist"
    else
      echo "wget $fastqR1" >> fastq_a_telecharger.sh
    fi
  fi
else
  echo "$DATE.ena.metadata.tsv doesn't exist."
fi
done

# chmod 755 fastq_a_telecharger.sh
# ./fastq_a_telecharger.sh
