#!/bin/bash

# utilisation de l'API de l'ENA pour récupérer l'ensemble des informations sur les échantillons de nos projets choisis

# récupération des informations sur un ensemble de projets

projets="PRJDB5807
PRJEB24540
PRJEB31325
PRJEB9534
PRJNA149115
PRJNA149155
PRJNA163649
"

# FIELDS="study_accession,secondary_study_accession,sample_accession,secondary_sample_accession,experiment_accession,run_accession,submission_accession,tax_id,scientific_name,instrument_platform,instrument_model,library_name,nominal_length,library_layout,library_strategy,library_source,library_selection,read_count,base_count,center_name,first_public,last_updated,experiment_title,study_title,study_alias,experiment_alias,run_alias,fastq_bytes,fastq_md5,fastq_ftp,sra_ftp,sample_alias,sample_title,nominal_sdev,first_created,sample_description,accession,cell_type,collected_by,collection_date,country,cultivar,description,dev_stage,ecotype,isolate,isolation_source,strain,tissue_type,variety,environment_biome,environment_feature,environment_material,temperature,project_name,host,host_tax_id,host_genotype,sample_material,lat,lon"
FIELDS="study_accession,secondary_study_accession,sample_accession,secondary_sample_accession,experiment_accession,run_accession,submission_accession,tax_id,scientific_name,instrument_platform,instrument_model,library_name,nominal_length,library_layout,library_strategy,library_source,library_selection,read_count,base_count,center_name,first_public,last_updated,experiment_title,study_title,study_alias,experiment_alias,run_alias,fastq_bytes,fastq_md5,fastq_ftp,sra_ftp,sample_alias,sample_title,nominal_sdev,first_created,sample_description,accession,cell_type,collected_by,collection_date,country,cultivar,description,dev_stage,ecotype,isolate,isolation_source,strain,tissue_type,variety,environment_biome,environment_feature,environment_material,temperature,project_name,host,host_tax_id,host_genotype,sample_material,lat,lon"

for projet in `echo $projets`
do
ACCESSION=$projet
curl -s "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=$FIELDS&accession=$ACCESSION" > $ACCESSION.ena.metadata.tsv
done

# récupération des informations sur un ensemble d'échantillons

SRRs="SRR866531
SRR866540
SRR866544
SRR866568
SRR866569"

FIELDS="study_accession,secondary_study_accession,sample_accession,secondary_sample_accession,experiment_accession,run_accession,submission_accession,tax_id,scientific_name,instrument_platform,instrument_model,library_name,nominal_length,library_layout,library_strategy,library_source,library_selection,read_count,base_count,center_name,first_public,last_updated,experiment_title,study_title,study_alias,experiment_alias,run_alias,fastq_bytes,fastq_md5,fastq_ftp,sra_ftp,sample_alias,sample_title,nominal_sdev,first_created,sample_description,accession,cell_type,collected_by,collection_date,country,cultivar,description,dev_stage,ecotype,isolate,isolation_source,strain,tissue_type,variety,environment_biome,environment_feature,environment_material,temperature,project_name,host,host_tax_id,host_genotype,sample_material,lat,lon"

for SRR in `echo $SRRs`
do
ACCESSION=$SRR
curl -s "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=$FIELDS&accession=$ACCESSION" >> $ACCESSION.ena.metadata.tsv
done

# récupération des fastq d'intérêts à partir d'une liste de SRR

SRRs="SRR866531
SRR866540
SRR866544
SRR866568
SRR866569"

for SRR in `echo $SRRs`
do
if [ -f "$SRR.ena.metadata.tsv" ]
then
  fastqR1=$( cut -f30 $SRR.ena.metadata.tsv | sed 1d | cut -f1 -d";" )
  fastqR2=$( cut -f30 $SRR.ena.metadata.tsv | sed 1d | cut -f2 -d";" )
  if [ -z "${fastqR1}" ]
  then
    echo "fastqR1 doesn't exist"
  else
    echo "wget $fastqR1" >> fastq_a_telecharger.sh
  fi
  if [ -z "${fastqR2}" ]
  then
    echo "fastqR2 doesn't exist"
  else
    echo "wget $fastqR2" >> fastq_a_telecharger.sh
  fi
else
  echo "$SRR.ena.metadata.tsv doesn't exist."
fi
done

# récupération des fastq d'intérêts à partir d'une liste de projets SRA

projets="PRJDB5807
PRJEB24540
PRJEB31325
PRJEB9534
PRJNA149115
PRJNA149155
PRJNA163649
"

for projet in `echo $projets`
do
if [ -f "$projet.ena.metadata.tsv" ]
then
  fastqR1=$( cut -f30 $projet.ena.metadata.tsv | sed 1d | cut -f1 -d";" | tr " " "\n" )
  fastqR2=$( cut -f30 $projet.ena.metadata.tsv | sed 1d | cut -f2 -d";" | tr " " "\n" )
  if [ -z "${fastqR1}" ]
  then
    echo "fastqR1 doesn't exist"
  else
    for file in `echo $fastqR1`
    do
    echo "wget $file" >> fastq_a_telecharger.sh
    done
  fi
  if [ -z "${fastqR2}" ]
  then
    echo "fastqR2 doesn't exist"
  else
    for file in `echo $fastqR2`
    do
    echo "wget $file" >> fastq_a_telecharger.sh
    done
  fi
else
  echo "$projet.ena.metadata.tsv doesn't exist."
fi
done

# lancer une recherche pour du RNAseq vitis vinifera
# a faire
https://www.ebi.ac.uk/ena/portal/api/search?result=read_run&query=scientific_name="United Kingdom" AND host_tax_id=9913 AND host_body_site="rumen"







# voici les champs généraux qui peuvent être accédés :

# pas intéressant pour nous car concerne les assemblages, dnaseq et autres
# resultId : description
# analysis_study : Studies used for nucleotide sequence analyses from reads
# analysis : Nucleotide sequence analyses from reads
# assembly : Genome assemblies
# coding : Coding sequences
# wgs_set : Genome assembly contig sets (WGS)
# tsa_set : Transcriptome assembly contig sets (TSA)
# tls_set : Targeted locus study contig sets (TLS)
# environmental : Environmental samples
# noncoding : Non-coding sequences
# sequence : Nucleotide sequences
# taxon : Taxonomic classification

# peuvent nous intéresser pour le RNAseq
# read_study : Studies used for raw reads
# read_experiment : Experiments used for raw reads
# read_run : Raw reads
# sample : Samples
# study : Studies

# Pour avoir la liste de toutes les infos de chaque champs :
# https://www.ebi.ac.uk/ena/portal/api/searchFields?result=read_study
# https://www.ebi.ac.uk/ena/portal/api/searchFields?result=read_experiment
# ...

# informations à récupérer :
# BioProject, First_author, Paper, File_name, Private_public?, Sequencing_platform, PE_SE, Read_length_bp, Protocol_library,
# Stranded, Species, Variety, Organ, Tissue, Short_stage, Detailed_stage, Experimentation_type, Treatment?, Treatment_type,
# Treatment_nature, Treatment_detail, Comments
