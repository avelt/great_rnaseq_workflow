#!/bin/bash

conda activate great

cd /home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow

# cd /home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/test_data/references_files
# cp /home/avelt/Archives/Data/PN12Xv2/VCost.v3/vitviv2.cds.fasta .
# # il n'y a aucun cds t02 donc j'enlève les .t01 pour avoir un ID de gène
# sed -i "s/.t01//" vitviv2.cds.fasta
# gzip vitviv2.cds.fasta

salmon index -t test_data/references_files/vitviv2.cds.fasta.gz -i vcostv3_salmon_index
