#!/bin/bash

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod 755 Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh

# example of path : /path/to/great_rnaseq_workflow/GREAT_env

export PATH=/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/bin/:$PATH
conda update --all --yes
conda env create -f GREAT_environment.yaml

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/2022_great_add_salmon/great_rnaseq_workflow/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


conda activate great
