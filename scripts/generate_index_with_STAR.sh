#!/bin/bash

conda activate great

STAR --runThreadN 5 --runMode genomeGenerate --genomeDir test_data/references_files/index --genomeFastaFiles test_data/references_files/PN12Xv2.fa \
--sjdbGTFfile test_data/references_files/VCost.v3_20.formatted.gff3 --sjdbGTFtagExonParentTranscript Parent --sjdbGTFtagExonParentGene GeneID --genomeSAindexNbases 13
