#!/bin/bash
export PATH=/path_to/great_rnaseq_workflow/GREAT_env/bin/:$PATH
conda activate great
cd /path_to/great_rnaseq_workflow/
snakemake --rerun-incomplete -s Snakefile --cores 4 &>> /path_to/great_rnaseq_workflow/GREAT.log
